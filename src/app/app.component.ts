import { Component } from '@angular/core';
import {takeUntilDestroyed} from '@angular/core/rxjs-interop';
import {FormControl, Validators, FormsModule, ReactiveFormsModule, FormGroup} from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {merge} from 'rxjs';

interface User {
  firstName: string;
  lastName: string;
  phoneNumber?: number;
  email: string;
}

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MatFormFieldModule, MatInputModule, MatIconModule, MatListModule, MatButtonModule, MatCardModule, FormsModule, ReactiveFormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  userForm = new FormGroup(
    {
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl(null, [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
    }
  );

  users: User[] = []; 

  errorMessage = '';

  constructor() {
    merge(this.userForm.controls.email.statusChanges, this.userForm.controls.email.valueChanges)
      .pipe(takeUntilDestroyed())
      .subscribe(() => this.updateErrorMessage());
  }

  onSubmit() {  
    if (this.userForm.valid ) {
      const user: User = this.userForm.value as User;
      this.users.push(user);
      this.users.sort((userA, userB) => userA.firstName.localeCompare(userB.firstName));
    }  
  }

  removeItem(index: number) {
    this.users.splice(index, 1);
  }

  updateErrorMessage() {
    if (this.userForm.controls.email.hasError('required')) {
      this.errorMessage = 'You must enter a value';
    } else if (this.userForm.controls.email.hasError('email')) {
      this.errorMessage = 'Not a valid email';
    } else {
      this.errorMessage = '';
    }
  }
}
